@extends('layout')

@section('css', '<link href="/css/prism.css" rel="stylesheet">')

@section('content')
<pre data-line="3-6" class="line-numbers"><code class="language-css">{{$cssFile}}</code></pre>
@stop

@section('scripts')
<script src="/js/prism.js"></script>
@stop