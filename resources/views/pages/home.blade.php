@extends('layout')


@section('content')

<header class="header">
	<div class="container small">

		<div class="logo">CSS Cleaner</div>
	
		<form action="/parseSite" method="GET" id="parseYourSite">
		{!! csrf_field() !!}
			
			<div class="no-gutter clearfix">
				<div class="col-md-9">
			    	<input type="text" class="form-control" name="site" placeholder="Your site url">
			    </div>
				<div class="col-md-3">
			        <button class="btn btn-primary btn-block" type="submit">Clean</button>
			    </div>
			</div>

		</form>

	</div>
</header>


<div class="container small">
	<div class="alert alert-danger cssAlert unvisible">
		Unfortunately css file in not valid
	</div>
</div>

<div class="container">

	<div class="loading unvisible">
		<img src="/img/loading.gif" alt="">
	</div>

	<ul class="stylesheetList unvisible list-unstyled">
	</ul>

</div>


@stop


