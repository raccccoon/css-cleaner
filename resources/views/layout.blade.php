<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CSS Cleaner</title>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/css/custom.css" rel="stylesheet">
	<link href="/css/prism.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,300,400" rel="stylesheet">
@yield('css')
</head>
<body>


	@yield('content')


	<script src="/js/jquery.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/functions.js"></script>
	<script src="/js/prism.js"></script>
	@yield('scripts')
</body>
</html>
