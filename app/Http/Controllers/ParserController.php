<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ParserController extends Controller
{
    public function parseSite(Request $data) {


    	//$pages = ["https://studhero.org", "https://studhero.org/employers"];
    	//$site = "http://ktonanovenkogo.ru/";

    	$site =  $data->site;
    	$sitemap = $site . "/sitemap.xml";
		$responseXmlData = file_get_contents($sitemap);
        $xmlData = simplexml_load_string($responseXmlData);
        $pages = [];
        
        foreach ($xmlData->children() as $loc) { 
            $pages[] = (string) $loc->loc; 
        }

        $pages = array_slice($pages, 0, 5);

    	// Array of objects 
    	$stylesheets = [];

    	$libStandarts = ["/css/bootstrap.min.css", "/css/bootstrap-select.css", "/css/font-awesome.min.css"];

    	foreach ($pages as $pageUrl) {
    		try{
	    		$stylesheetAddresses = self::getStylesheetElements($pageUrl);

	    		$page = file_get_contents($pageUrl);
	   			$html = str_get_html($page);

		    	// Get CSS
		    	foreach($stylesheetAddresses as $style) {

		    		if (!in_array($style->href, $libStandarts)) {

			    		if (array_key_exists($style->href, $stylesheets)) {
		    				$selectors = $stylesheets[$style->href]->unusedSelectors;
		    				$stylesheets[$style->href]->unusedSelectors = self::getUnusedSelectors($selectors, $html);
			    		}
			    		else {
				    		$cssObject = new \stdClass();
				       		// $cssObject->css = str_replace("\n", "", file_get_contents($site . $style->href));
				       		$cssObject->css = file_get_contents($style->href);
			    			$selectors = self::parseCssOnSelectors($cssObject->css);
				       		$cssObject->unusedSelectors = self::getUnusedSelectors($selectors, $html);
				       		$stylesheets[$style->href] = $cssObject;
			    		}
			    	}

		    	}
		    }
		    catch(\Exception $e){
	    		return json_encode(["status" => "Error", "response" => $pageUrl]);
	    	}
    	}


    	foreach ($stylesheets as $stylesheet) {
    		$stylesheet->unusedCss = "";

    		foreach ($stylesheet->unusedSelectors as $unused) {
				$start = strpos($stylesheet->css, $unused);
		        $end = strpos($stylesheet->css, "}", $start) + 1;
		        $length = $end - $start;
		        $stylesheet->unusedCss .= substr($stylesheet->css, $start, $length);
		        $stylesheet->css = substr_replace($stylesheet->css, "", $start, $length);
    		}
    	}


    	return json_encode(["status" => "Success", "response" => $stylesheets]);
    }

    private function parseCssOnSelectors($css) {
    	$cssParser = new \Sabberworm\CSS\Parser($css);
		$cssDocument = $cssParser->parse();
		$allCssSelectors = $cssDocument->getAllSelectors();

		$array = array_map(function($element) { return $element->getSelectors(); }, $allCssSelectors);

		return $array;
    }

    private function getStylesheetElements($site) {
    	$page = file_get_contents($site);
    	$html = str_get_html($page);
    	return $html->find("link[rel=stylesheet]");
    }

    private function getUnusedSelectors($allCssSelectors, $html) {
    	
		// Check if exists
		$selectors = [];

		foreach($allCssSelectors as $selector) {
			$pseudoClass = strpos($selector, ':');

			if (!$pseudoClass) {
		    	$selectors[] = $selector;
		    }
		}

		$selectors = array_unique($selectors);

		// Check if exists in HTML
    	foreach ($selectors as $index => $selector) {
    		if (!empty($html->find($selector))) {
    			unset($selectors[$index]);
    		}
    	}

    	return $selectors;
    }
}
