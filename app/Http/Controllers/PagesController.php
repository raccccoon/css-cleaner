<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller{

    public function home() {


    	return view('pages.home');
    }

    public function cssPreview(){
    	
    	$cssFile = file_get_contents("https://studhero.org/css/custom.css");


    	return view('pages.cssPreview')
    		->with('cssFile', $cssFile);
    }
}
