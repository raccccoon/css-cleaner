$(document).ready(function() {


    $("#parseYourSite").submit(function(e) {

    	$(".loading").show();

    	var form = $(this);

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function(response) {
				$(".loading").hide();

                var data = JSON.parse(response);

                if(data.status == "Success"){
                    $(".cssAlert").hide();

                    var styles = data.response;

                    for (var style in styles) {
                        var css = styles[style].css;
                    	var unusedCss = styles[style].unusedCss;
                        css.replace('\n', '');
                        unusedCss.replace('\n', '');
                        // console.log(css);
                        var element = "<li class='row learfix'>" + 
                            "<div class='col-md-6'><h2>Cleaned Css " + style + "</h2><pre class='line-numbers'><code class='language-css'>" + css + "</code></pre></div>" +
                        	"<div class='col-md-6'><h2>Unused Css " + style + "</h2><pre class='line-numbers'><code class='language-css'>" + unusedCss + "</code></pre></div>" +
                            "</li>";

                    	$(".stylesheetList").append(element).show();
    				}
                }
                else{
                    $(".cssAlert").show();
                }
            }
        });

        e.preventDefault();
    });



});